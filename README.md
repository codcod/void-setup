# Void setup

Based on the work of [eoli3n](https://github.com/eoli3n/void-config/tree/master/scripts/install), adjusted to my needs.

## Prepare

Boot latest [hrmpf rescue system](https://github.com/leahneukirchen/hrmpf/releases/latest).

Setup wifi:

	# wpa_supplicant -i wlan0 -c /etc/wpa_supplican/wpa_supplicant.conf -B
	# wpa_cli -i wlan0
	> scan
	> scan_results
	> add_network
	> set_network 0 ssid "<mynetwork>"
	> set network 0 psk "<mypassword>"
	> enable_network 0
	> quit
	# dhcpcd -i wlan0

	# git clone && cd
	# ./01-configure.sh
	# ./02-install.sh


Tested on Thinkpad T440p.
